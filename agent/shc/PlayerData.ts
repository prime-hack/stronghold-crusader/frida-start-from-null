import { Resources } from "./Resources.js";

export class PlayerData {
    readonly base: NativePointer;
    static readonly Size = 14836;

    // 0x115F7EC - local player on eng version, 0x115BDF8 - start player pool
    constructor(ptr: NativePointer) {
        this.base = ptr;
    }

    Reputation(): number {
        return this.base.add(96).readInt() / 100;
    }
    MaxReputation(): number {
        return this.base.add(116).readU8();
    }

    WaitResources(): Resources {
        return new Resources(this.base.add(1132));
    }
    Resources(): Resources {
        return new Resources(this.base.add(1232));
    }

    Population(): number {
        return this.base.add(8576).readInt();
    }
    PlagueFactor(): number {
        return this.base.add(8876).readS16();
    }
    LionFactor(): number {
        return this.base.add(8878).readS16();
    }
    BanditsFactor(): number {
        return this.base.add(8880).readS16();
    }
    FireFactor(): number {
        return this.base.add(8882).readS16();
    }
    /// Positive
    WeddingFactor(): number {
        return this.base.add(8884).readS16();
    }
    /// Positive
    JesterFactor(): number {
        return this.base.add(8886).readS16();
    }

    SetReputation(value: number): void {
        this.base.add(96).writeInt(value * 100);
    }
    SetMaxReputation(value: number): void {
        this.base.add(116).writeU8(value);
    }
    SetPopulation(value: number): void {
        this.base.add(8576).writeInt(value);
    }
    SetPlagueFactor(value: number): void {
        this.base.add(8876).writeS16(value);
    }
    SetLionFactor(value: number): void {
        this.base.add(8878).writeS16(value);
    }
    SetBanditsFactor(value: number): void {
        this.base.add(8880).writeS16(value);
    }
    SetFireFactor(value: number): void {
        this.base.add(8882).writeS16(value);
    }
    SetWeddingFactor(value: number): void {
        this.base.add(8884).writeS16(value);
    }
    SetJesterFactor(value: number): void {
        this.base.add(8886).writeS16(value);
    }
};
