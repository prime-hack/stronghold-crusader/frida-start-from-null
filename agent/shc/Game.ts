import { PlayerData } from "./PlayerData.js";

export enum PlayerID {
    Viewer = 0,
    Red, /// Local player by default
    Orange,
    Yellow,
    Blue,
    Gray,
    Violet,
    LightBlue,
    Green,

    Count
};

export class Game {
    readonly base: NativePointer;
    static readonly Size = 335652; // Not full

    constructor(ptr: NativePointer) {
        this.base = ptr;
    }

    Players(id: PlayerID): PlayerData {
        return new PlayerData(this.base.add(200000 + id * PlayerData.Size));
    }

    NeedResetPlayers(): boolean {
        return this.base.add(334348).readInt() != 0;
    }
    SetNeedResetPlayers(value: boolean): void {
        this.base.add(334348).writeInt(value ? 1 : 0);
    }
};
