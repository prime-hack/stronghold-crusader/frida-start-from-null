export class Resources {
    readonly base: NativePointer;
    static readonly Size = 100;

    constructor(ptr: NativePointer) {
        this.base = ptr;
    }

    Wood(): number {
        return this.base.add(8).readInt();
    }
    Hop(): number {
        return this.base.add(12).readInt();
    }
    Stone(): number {
        return this.base.add(16).readInt();
    }
    Iron(): number {
        return this.base.add(24).readInt();
    }
    Oil(): number {
        return this.base.add(28).readInt();
    }
    Corn(): number {
        return this.base.add(36).readInt();
    }
    Eat0(): number {
        return this.base.add(40).readInt();
    }
    Eat1(): number {
        return this.base.add(44).readInt();
    }
    Eat2(): number {
        return this.base.add(48).readInt();
    }
    Eat3(): number {
        return this.base.add(52).readInt();
    }
    Bear(): number {
        return this.base.add(56).readInt();
    }
    Money(): number {
        return this.base.add(60).readInt();
    }
    Meal(): number {
        return this.base.add(64).readInt();
    }
    Weapon0(): number {
        return this.base.add(68).readInt();
    }
    Weapon1(): number {
        return this.base.add(72).readInt();
    }
    Weapon2(): number {
        return this.base.add(76).readInt();
    }
    Weapon3(): number {
        return this.base.add(80).readInt();
    }
    Weapon4(): number {
        return this.base.add(84).readInt();
    }
    Weapon5(): number {
        return this.base.add(88).readInt();
    }
    Weapon6(): number {
        return this.base.add(92).readInt();
    }
    Weapon7(): number {
        return this.base.add(96).readInt();
    }


    SetWood(value: number): void {
        this.base.add(8).writeInt(value);
    }
    SetHop(value: number): void {
        this.base.add(12).writeInt(value);
    }
    SetStone(value: number): void {
        this.base.add(16).writeInt(value);
    }
    SetIron(value: number): void {
        this.base.add(24).writeInt(value);
    }
    SetOil(value: number): void {
        this.base.add(28).writeInt(value);
    }
    SetCorn(value: number): void {
        this.base.add(36).writeInt(value);
    }
    SetEat0(value: number): void {
        this.base.add(40).writeInt(value);
    }
    SetEat1(value: number): void {
        this.base.add(44).writeInt(value);
    }
    SetEat2(value: number): void {
        this.base.add(48).writeInt(value);
    }
    SetEat3(value: number): void {
        this.base.add(52).writeInt(value);
    }
    SetBear(value: number): void {
        this.base.add(56).writeInt(value);
    }
    SetMoney(value: number): void {
        this.base.add(60).writeInt(value);
    }
    SetMeal(value: number): void {
        this.base.add(64).writeInt(value);
    }
    SetWeapon0(value: number): void {
        this.base.add(68).writeInt(value);
    }
    SetWeapon1(value: number): void {
        this.base.add(72).writeInt(value);
    }
    SetWeapon2(value: number): void {
        this.base.add(76).writeInt(value);
    }
    SetWeapon3(value: number): void {
        this.base.add(80).writeInt(value);
    }
    SetWeapon4(value: number): void {
        this.base.add(84).writeInt(value);
    }
    SetWeapon5(value: number): void {
        this.base.add(88).writeInt(value);
    }
    SetWeapon6(value: number): void {
        this.base.add(92).writeInt(value);
    }
    SetWeapon7(value: number): void {
        this.base.add(96).writeInt(value);
    }

    Reset(): void {
        for (let i = 0; i < 25; i++)
            this.base.add(i * 4).writeInt(0);
    }
};
