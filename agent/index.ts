import { Game } from "./shc/Game.js";

Process.enumerateModules().forEach((module) => {
    if (module.name.toLowerCase().lastIndexOf(".exe") == -1)
        return;

    const reset_players = Memory.scanSync(module.base, module.size, "83 EC 08 53 55 8B E9 56 B8 ?? ?? ?? ?? 2B C5 B9 01 00 00 00 57 89 6C 24 10 8B D9");
    if (reset_players.length == 0) {
        console.error("Bad pattern to find player initializer - no matches found");
        return;
    } else if (reset_players.length > 1) {
        console.error("Bad pattern to find player initializer - found multiple places:");
        reset_players.forEach((p) => {
            console.error(" - ", p.address);
        })
        return;
    }

    const local_player_index_ptr = reset_players[0].address.add(146).readPointer();
    const game = new Game(reset_players[0].address.add(158).readPointer().sub(0x32ecc));

    Interceptor.attach(reset_players[0].address, {
        onLeave() {
            const local_player = game.Players(local_player_index_ptr.readS32());

            local_player.SetReputation(0);
            local_player.WaitResources().Reset();
        },
    })
})
