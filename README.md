Frida script to remove all resources and reputation for player on mission start

![](image.png)

# Simple usage

Enter `Stronghold Crusader` folder

```shell
git clone https://gitlab.com/prime-hack/stronghold-crusader/frida-start-from-null
frida -f 'Stronghold Crusader.exe' -l frida-start-from-null/agent/index.ts
```



If you have error **Failed to spawn: unable to handle 32-bit processes due to build configuration**:

1. Download frida-server for Windows-x86_64 from [here](https://github.com/frida/frida/releases/latest)
2. Start downloaded frida-server
3. Launch game, but do not start a mission
4. Attach script to game process: `frida -H 127.0.0.1 'Stronghold Crusader.exe' -l frida-start-from-null/agent/index.ts`
5. Start any mission or skirmish



# Hacking

Compile script and install all dependencies:

```shell
npm i
```

Now you can open project in editor and modify code. E.g. in VSCode.



For VSCode you also can run game from tasks:

```json
{
  "version": "2.0.0",
  "tasks": [
    {
      "isBackground": true,
      "label": "Launch game",
      "command": "frida",
      "args": [
        "${workspaceFolder}/../Stronghold Crusader.exe",
        "-l",
        "${workspaceFolder}/_agent.js",
        "--runtime=v8"
      ],
      "type": "shell"
    },
    {
      "isBackground": true,
      "label": "Connect to game",
      "command": "frida",
      "args": [
        "-H",
        "127.0.0.1",
        "Stronghold Crusader.exe",
        "-l",
        "${workspaceFolder}/_agent.js",
        "--runtime=v8"
      ],
      "type": "shell"
    }
  ]
}

```

